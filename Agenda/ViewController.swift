//
//  ViewController.swift
//  Agenda
//
//  Created by Mattei on 22/06/16.
//  Copyright © 2016 Mattei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var nuovoElemento:ElementoToDo?
    @IBOutlet  var cmdOK: UIBarButtonItem!
    @IBOutlet  var txtNuovoElemento: UITextField!
    override func viewDidLoad() {

        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if sender as? NSObject != self.cmdOK{return}
        if self.txtNuovoElemento.text!.utf16.count > 0 {
            self.nuovoElemento = ElementoToDo(nomeElemento: self.txtNuovoElemento.text!)
        }
    }
    
// fine classe
}

