//
//  ElementoToDo.swift
//  Agenda
//
//  Created by Mattei on 25/06/16.
//  Copyright © 2016 Mattei. All rights reserved.
//

import UIKit

class ElementoToDo: NSObject {
    var nomeElemento: NSString=""
    var flagCompletato: Bool=false
    var codice=""
    var dataCreazione: NSDate=NSDate()
    
    init(nomeElemento:String){
        self.nomeElemento=nomeElemento
    }

}
