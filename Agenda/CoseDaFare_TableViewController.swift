//
//  CoseDaFare_TableViewController.swift
//  Agenda
//
//  Created by Mattei on 22/06/16.
//  Copyright © 2016 Mattei. All rights reserved.
//

import UIKit

class CoseDaFare_TableViewController: UITableViewController {
    var arrayToDo: NSMutableArray=[]
    //var vet=[ElementoToDo]()
    @IBOutlet var tabella: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
      //  caricaDatiIniziali()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    
    func caricaDatiIniziali(){
        let elemento1=ElementoToDo(nomeElemento:"Uno")
        self.arrayToDo.addObject(elemento1)
        let elemento2=ElementoToDo(nomeElemento:"Due")
        self.arrayToDo.addObject(elemento2)
    }
    
    
    @IBAction func tornaAllaLista(segue: UIStoryboardSegue){
        let vistaDettaglio: ViewController=segue.sourceViewController as! ViewController
        if vistaDettaglio.nuovoElemento != nil {
            self.arrayToDo.addObject(vistaDettaglio.nuovoElemento!)
            self.tabella.reloadData()
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.arrayToDo.count
    }

    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("SingolaCe", forIndexPath: indexPath) as UITableViewCell
        print("Sono in dequeue, appena prima di fare addObject al vettore in posizione:")
        print (indexPath.row)
        let elemento:ElementoToDo=self.arrayToDo.objectAtIndex(indexPath.row) as! ElementoToDo
        
        cell.textLabel!.text=elemento.nomeElemento as String
        if elemento.flagCompletato {
            cell.accessoryType = .Checkmark
        }
        else
        {
            cell.accessoryType = .None
        }

            return cell
        
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath,animated: false)
        let selezionato: ElementoToDo=self.arrayToDo.objectAtIndex(indexPath.row) as! ElementoToDo
        selezionato.flagCompletato = !(selezionato.flagCompletato)
        tabella.reloadData()
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
